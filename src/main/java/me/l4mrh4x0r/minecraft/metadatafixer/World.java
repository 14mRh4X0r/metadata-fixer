package me.l4mrh4x0r.minecraft.metadatafixer;

import net.minecraft.world.level.chunk.storage.RegionFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.stream.Stream;

/**
 * Created by willem on 28/01/17.
 */
public class World {

    public static boolean isRegionFile(Path path, BasicFileAttributes attrs) {
        return attrs.isRegularFile() && path.toString().endsWith(".mca");
    }

    public static void main(String[] args) throws IOException {
        if (args.length != 1) {
            System.err.println("Usage: World <path>");
            System.exit(1);
        }

        Path worldPath = Paths.get(args[0]);
        Stream.of(worldPath, worldPath.resolve("DIM-1"), worldPath.resolve("DIM1"))
                .flatMap(Util.unchecked(p -> Files.find(p.resolve("region"), 1, World::isRegionFile)))
                //.forEach(System.out::println);
                .parallel()
                .map(Path::toFile)
                .map(Region::new)
                .forEach(Region::fixMetadata);
    }
}
