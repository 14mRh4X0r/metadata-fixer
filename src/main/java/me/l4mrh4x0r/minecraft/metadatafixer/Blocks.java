package me.l4mrh4x0r.minecraft.metadatafixer;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.HashSet;

/**
 * Created by willem on 28/01/17.
 */
public class Blocks {
    public static final URL BLOCKS_JSON = getURL("https://github.com/Thinkofname/mc-autodocs/raw/master/blocks.json");
    public static final Map<Byte, Set<Byte>> ALLOWED_META = getAllowedMeta();

    private static URL getURL(String url) {
        try {
            return new URL(url);
        } catch (MalformedURLException e) {
            throw new RuntimeException("Failed to parse URL (wtf?)", e);
        }
    }

    private static Map<Byte, Set<Byte>> getAllowedMeta() {
        HashMap<Byte, Set<Byte>> allowedMeta = new HashMap<>();
        
        try (JsonParser parser = new JsonFactory(new ObjectMapper()).createParser(BLOCKS_JSON)) {
            parser.nextToken(); // Discard object start
            while (parser.nextToken() == JsonToken.FIELD_NAME) {
                String[] parts = parser.getCurrentName().split(":", 2);
                byte block = (byte) Integer.parseInt(parts[0]);
                byte data = (byte) Integer.parseInt(parts[1]);

                Set<Byte> dataSet = allowedMeta.get(block);
                if (dataSet == null) {
                    dataSet = new HashSet<>();
                    allowedMeta.put(block, dataSet);
                }
                dataSet.add(data);

                // Discard value
                parser.nextToken();
                parser.readValueAsTree();
            }
        } catch (IOException e) {
            throw new RuntimeException("Could not parse allowed metadata", e);
        }

        return allowedMeta;
    }

}
