package me.l4mrh4x0r.minecraft.metadatafixer;

import com.mojang.nbt.CompoundTag;
import com.mojang.nbt.ListTag;
import com.mojang.nbt.Tag;
import net.minecraft.world.level.chunk.storage.RegionFile;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by willem on 28/01/17.
 */
public class Region {
    private final class Coord {
        public final int x, z;

        public Coord(int x, int z) {
            this.x = x;
            this.z = z;
        }

        @Override
        public String toString() {
            return "(" + this.x + "," + this.z + ")";
        }
    }

    private final String fileName;
    private final RegionFile regionFile;

    public Region(File file) {
        this.fileName = file.getName();
        this.regionFile = new RegionFile(file);
    }

    public void fixMetadata() {
        System.out.println("Fixing metadata of region " + this.fileName);
        IntStream.range(0, 32).mapToObj(x ->
                IntStream.range(0, 32).mapToObj(z -> new Coord(x, z))
        ).flatMap(Function.identity())
                .parallel()
                .filter(coord -> regionFile.hasChunk(coord.x, coord.z))
                .forEach(this::fixChunk);
    }

    private void fixChunk(Coord coord) {
        //System.out.println("Fixing chunk at " + coord);
        // Opening both input and output is safe because output only gets written on close
        try (DataInputStream dis = regionFile.getChunkDataInputStream(coord.x, coord.z);
                DataOutputStream dos = regionFile.getChunkDataOutputStream(coord.x, coord.z)) {
            CompoundTag chunk = (CompoundTag) Tag.readNamedTag(dis);
            @SuppressWarnings("unchecked")
            ListTag<CompoundTag> sections = (ListTag<CompoundTag>) chunk.getCompound("Level").getList("Sections");

            // Parallelize
            IntStream.range(0, sections.size())
                    .parallel()
                    .mapToObj(sections::get)
                    .forEach(Region::fixSection);

            Tag.writeNamedTag(chunk, dos);
        } catch (IOException e) {
            throw new RuntimeException("Could not convert chunk at " + coord, e);
        }
    }

    private static void fixSection(CompoundTag section) {
        byte[] blocks = section.getByteArray("Blocks");
        byte[] dataNibbles = section.getByteArray("Data");

        IntStream.range(0, blocks.length)
                .parallel()
                .forEach(i -> {
            Set<Byte> allowedMeta = Blocks.ALLOWED_META.get(blocks[i]);
            byte data = Util.getNibble(dataNibbles, i);
            if (!allowedMeta.contains(data)) {
                if (allowedMeta.contains((byte) (data & 0x7))) {
                    Util.setNibble(dataNibbles, i, (byte) (data & 0x7));
                } else {
                    Util.setNibble(dataNibbles, i, allowedMeta.iterator().next());
                }
            }
        });
    }

    public static void main(String[] args) {
        Stream.of(args)
                .parallel()
                .map(File::new)
                .map(Region::new)
                .forEach(Region::fixMetadata);
    }
}
