package me.l4mrh4x0r.minecraft.metadatafixer;

import java.util.function.Function;

/**
 * Created by willem on 28/01/17.
 */
public class Util {
    private Util() {
        throw new UnsupportedOperationException("Not meant to be instantiated");
    }

    @FunctionalInterface
    public interface UncheckedFunction<T, R> {
        public R apply(T t) throws Exception;
    }

    public static <T, R> Function<T, R> unchecked(UncheckedFunction<T, R> func) {
        return t -> {
            try {
                return func.apply(t);
            } catch (Exception e) {
                throw new RuntimeException("Exception while applying function", e);
            }
        };
    }

    public static byte getNibble(byte[] array, int nibbleIndex) {
        byte dataPoint = array[nibbleIndex/2];
        return (byte) ((nibbleIndex % 2 == 0 ? dataPoint : dataPoint >> 4) & 0xf);
    }

    public static void setNibble(byte[] array, int nibbleIndex, byte value) {
        value &= 0xf;
        array[nibbleIndex/2] &= (nibbleIndex % 2 == 0 ? 0xf0 : 0x0f); // Clear bits to set
        array[nibbleIndex/2] |= (nibbleIndex % 2 == 0 ? value : value << 4);
    }
}
